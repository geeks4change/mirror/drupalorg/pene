Panels Everywhere Not Everywhere (PENE)
=======================================

Contrary to Panels Everywhere (which hijacks the whole screen with its page template), PENE
only hijacks the content area. This allows to combine the Panels Everywhere page template with the
well-known blocks and regions system.

How to use
----------

* Install Panels Everywhere and PENE
* Go to admin/structure/panels/settings/everywhere. enable the site template, 
  but prevent the page from being hijacked:
    * Check _Enable Panels Everywhere site template_
    * Check _Enable Panels Everywhere site template on a per-theme basis_
    * Uncheck all Themes
* Go to admin/structure/panels/settings/pene and enable the the themes for which you want to 
  hijack the content area block

Other modules useful with this
------------------------------

* [Pane Mover](https://www.drupal.org/project/panemover)
* [Panels Content first](https://www.drupal.org/project/panels_cf)
