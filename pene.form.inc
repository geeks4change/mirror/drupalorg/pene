<?php
/**
 * @file pene.form.inc
 */

/**
 * Form callback.
 */
function pene_settings_form($form, &$form_state) {
  $form['pene_overview'] = array(
    '#type' => 'item',
    '#title' => t('PENE overview'),
    '#description' => t(
      'Panels Everywhere Not Everywhere will take the Panels Everywhere page template output and expose it in the content block. '
      . 'This way you can have the best of both worlds. You may want to use it with panemover module.'
    )
  );
  $themes = list_themes();

  foreach ($themes as $theme) {
    if (empty($theme->info['hidden'])) {
      $form['pene_theme_' . $theme->name] = array(
        '#type' => 'checkbox',
        '#title' => t($theme->info['name']),
        '#default_value' => variable_get('pene_theme_' . $theme->name, FALSE),
      );
    }
  }

  $form = system_settings_form($form);
  return $form;
}
